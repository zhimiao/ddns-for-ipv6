package main

import (
	"fmt"
	"log"
	"time"
)

var lastIp string

func main() {
	err := LoadConfig("config.toml")
	if err != nil {
		log.Fatal(err.Error())
	}
	alidnsClient, err := NewAlidnsClient()
	if err != nil {
		log.Fatal(err.Error())
	}
	conf := GetConfig()
	ticker := time.NewTicker(time.Duration(conf.ScanTicker) * time.Second)
	defer ticker.Stop()
	for range ticker.C {
		ips, _ := getIP()
		if len(ips) > 0 && lastIp != ips[0] {
			fmt.Printf("Ip发生变化，正在更新[%s.%s]解析值为[%s]\n", conf.RR, conf.DomainName, ips[0])
			if err := alidnsClient.SetNewIPV6(ips[0]); err != nil {
				fmt.Println("更新失败，请检查配置")
			}
			lastIp = ips[0]
		}
	}
}
