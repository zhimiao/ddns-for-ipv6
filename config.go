package main

import (
	"github.com/jinzhu/configor"
)

type configStruct struct {
	AccessKey     string
	AccessSecret  string
	RegionId      string `default:"cn-hangzhou"`
	RR            string
	DomainName    string
	CleanRepeatRR bool `defalut:"false"`
	ScanTicker    int  `default:"3"`
}

var cfg = &configStruct{}

// Init 初始化配置
func LoadConfig(filePath string) error {
	return configor.Load(cfg, filePath)
}

// GetConfig 获取配置
func GetConfig() *configStruct {
	return cfg
}

// ENV 获取当前配置场景
func ENV() string {
	return configor.ENV()
}
