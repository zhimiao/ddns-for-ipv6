package main

import (
	"errors"
	"fmt"

	"github.com/aliyun/alibaba-cloud-sdk-go/services/alidns"
)

const v6Type = "AAAA"

type alidnsStruct struct {
	client *alidns.Client
	conf   *configStruct
}

func NewAlidnsClient() (*alidnsStruct, error) {
	conf := GetConfig()
	client, err := alidns.NewClientWithAccessKey(conf.RegionId, conf.AccessKey, conf.AccessSecret)
	if err != nil {
		return nil, err
	}
	return &alidnsStruct{
		client: client,
		conf:   conf,
	}, nil
}

func (c *alidnsStruct) SetNewIPV6(ip string) error {
	if ip == "" {
		return nil
	}
	old, err := c.GetOldRRID()
	if err != nil {
		return err
	}
	if old == nil || old.RecordId == "" {
		request := alidns.CreateAddDomainRecordRequest()
		request.DomainName = c.conf.DomainName
		request.RR = c.conf.RR
		request.Type = v6Type
		request.Value = ip
		_, err := c.client.AddDomainRecord(request)
		if err != nil {
			return err
		}
		return nil
	}
	if old.Value == ip {
		fmt.Println("重复记录修改，跳过")
		return nil
	}
	request := alidns.CreateUpdateDomainRecordRequest()
	request.Scheme = "https"
	request.RecordId = old.RecordId
	request.RR = c.conf.RR
	request.Type = v6Type
	request.Value = ip
	_, err = c.client.UpdateDomainRecord(request)
	if err != nil {
		return err
	}
	return nil
}

func (c *alidnsStruct) GetOldRRID() (*alidns.Record, error) {
	if c.conf.RR == "" {
		return nil, errors.New("RR Empty")
	}
	listRequest := alidns.CreateDescribeDomainRecordsRequest()
	listRequest.Scheme = "https"
	listRequest.DomainName = c.conf.DomainName
	listRequest.PageSize = "500"
	listRequest.RRKeyWord = c.conf.RR
	listResponse, err := c.client.DescribeDomainRecords(listRequest)
	if err != nil {
		return nil, err
	}
	var old alidns.Record
	for _, v := range listResponse.DomainRecords.Record {
		if v.Type == v6Type {
			old = v
		} else if c.conf.CleanRepeatRR {
			fmt.Printf("发现重名%s主机记录，正在尝试删除：", v.Type)
			if err := c.DeleteRR(v.RecordId); err != nil {
				fmt.Println("删除失败")
			} else {
				fmt.Println("删除成功")
			}
		}
	}
	return &old, nil
}

func (c *alidnsStruct) DeleteRR(id string) error {
	request := alidns.CreateDeleteDomainRecordRequest()
	request.Scheme = "https"
	request.RecordId = id
	response, err := c.client.DeleteDomainRecord(request)
	if err != nil {
		return err
	}
	if !response.IsSuccess() {
		return errors.New("删除失败")
	}
	return nil
}
