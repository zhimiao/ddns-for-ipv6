module gitee.com/zhimiao/ddns-for-ipv6

go 1.13

require (
	github.com/aliyun/alibaba-cloud-sdk-go v1.61.486
	github.com/jinzhu/configor v1.2.0
)
