package main

import (
	"net"
	"strings"
)

func getIP() ([]string, error) {
	netInterfaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	data := make([]string, 0)
	// data = append(data, "240e:3a1:c37:33d1:846b:21e7:feee:5013")
	for i := 0; i < len(netInterfaces); i++ {
		if (netInterfaces[i].Flags & net.FlagUp) != 0 {
			adders, _ := netInterfaces[i].Addrs()
			for _, address := range adders {
				if ipNet, ok := address.(*net.IPNet); ok {
					if !ipNet.IP.IsLoopback() &&
						ipNet.IP.To16() != nil &&
						strings.Contains(ipNet.IP.String(), ":") &&
						ipNet.Mask.String() == "ffffffffffffffffffffffffffffffff" {
						data = append(data, ipNet.IP.String())
					}
				}
			}
		}
	}
	return data, nil
}
